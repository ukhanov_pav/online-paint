import React, { useState } from 'react';
import '../styles/toolbar.scss'
import toolState from '../store/toolState';
import canvasState from '../store/canvasState';
import Brush from '../tools/Brush';
import Rect from '../tools/Rect';
import Eraser from '../tools/Eraser';
import Circle from '../tools/Circle';
import Line from '../tools/Line';
import { Button } from 'antd'
import { DownloadOutlined, UndoOutlined, RedoOutlined } from '@ant-design/icons';


const Toolbar = () => {
    const [tool, setTool] = useState('brush');
    const changeColor = e => {
        toolState.setStrokeColor(e.target.value);
        toolState.setFillColor(e.target.value);
    }

    const download = () => {
        const dataUrl = canvasState.canvas.toDataURL();
        const a = document.createElement('a');
        a.href = dataUrl
        a.download = canvasState.sessionId + ".jpg";
        document.body.appendChild(a);
        a.click()
        document.body.removeChild(a);
    }

    return (
        <div className='toolbar'>
            <button title="Кисть" className={'toolbar__btn brush' + (tool === 'brush' ? ' active' : '')} onClick={() =>{ toolState.setTool(new Brush(canvasState.canvas, canvasState.socket, canvasState.sessionId)); setTool('brush')}}></button>
            <button title="Квадрат" className={`toolbar__btn rect` + (tool === 'rect' ? ' active' : '')} onClick={() => {toolState.setTool(new Rect(canvasState.canvas, canvasState.socket, canvasState.sessionId)); setTool('rect')}}></button>
            <button title="Круг" className={`toolbar__btn circle` + (tool === 'circle' ? ' active' : '')} onClick={() => {toolState.setTool(new Circle(canvasState.canvas, canvasState.socket, canvasState.sessionId)); setTool('circle')}}></button>
            <button title="Стереть" className={`toolbar__btn eraser` + (tool === 'eraser' ? ' active' : '')} onClick={() => {toolState.setTool(new Eraser(canvasState.canvas, canvasState.socket, canvasState.sessionId)); setTool('eraser')}}></button>
            <button title="Прямая" className={`toolbar__btn line`+ (tool === 'line' ? ' active' : '')} onClick={() => {toolState.setTool(new Line(canvasState.canvas, canvasState.socket, canvasState.sessionId)); setTool('line')}}></button>
            <input title="Цвет заливки" onChange={e => changeColor(e)} style={{ marginLeft: 10 }} type="color"></input>


            <Button title="Вернуть" className='toolbar__btn-antd undo' icon={<UndoOutlined />} onClick={() => canvasState.undo()} />
            <Button title="Вперед" className='toolbar__btn-antd redo' icon={<RedoOutlined />} onClick={() => canvasState.redo()} />
            <Button className='toolbar__btn-antd save' type="primary" icon={<DownloadOutlined />} onClick={() => download()}> Сохранить </Button>
        </div>
    );
};

export default Toolbar;