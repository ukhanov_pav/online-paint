import Tool from "./Tool";

export default class CircleWithinCircle extends Tool{
    constructor(canvas) {
        super(canvas);
        this.listen();
    }

    listen() {
        this.canvas.onmousemove = this.mouseMoveHandler.bind(this);
        this.canvas.onmousedown = this.mouseDownHandler.bind(this);
        this.canvas.onmouseup = this.mouseUpHandler.bind(this);
    }

    disance(startX, startY, currentX, currentY) {
        return Math.sqrt(((startX - currentX) ** 2) + ((startY - currentY) ** 2));
    }

    mouseUpHandler(e) {
        this.mouseDown = false;
    }
    mouseDownHandler(e) {
        this.mouseDown = true;
        this.ctx.beginPath();
        this.startX = e.pageX - e.target.offsetLeft;
        this.startY = e.pageY - e.target.offsetTop;
        this.saved = this.canvas.toDataURL();
    }
    mouseMoveHandler(e) {
        if (this.mouseDown) {
            let currentX = e.pageX - e.target.offsetLeft;
            let currentY = e.pageY - e.target.offsetTop;
            let currentRadius = this.disance(this.startX, this.startY, currentX, currentY)
            this.draw(this.startX, this.startY, currentRadius);
        }
    }

    draw(x, y, r) {
        const img = new Image();
        img.src = this.saved;
        img.onload = () => {
            this.ctx.beginPath();
            this.ctx.arc(x,y,r,0*Math.PI,2*Math.PI);
            this.ctx.stroke();
            console.log('draw circle');
        }
    }

}