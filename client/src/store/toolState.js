import { makeAutoObservable } from "mobx";

class ToolState {
    tool = null;

    constructor() {
        makeAutoObservable(this) // сделает данные хранящиеся в данном классе отслеживаемыми
    }

    setTool(tool) { // функция называется action, так называются функции которые как то изменяют состояния
        this.tool = tool;
    }

    setFillColor(color) { // функция называется action, так называются функции которые как то изменяют состояния
        this.tool.fillColor = color;
    }

    setStrokeColor(color) { // функция называется action, так называются функции которые как то изменяют состояния
        this.tool.strokeColor = color;
    }

    setLineWidth(width) { // функция называется action, так называются функции которые как то изменяют состояния
        this.tool.lineWidth = width;
    }
}

export default new ToolState()